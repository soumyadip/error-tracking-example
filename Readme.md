
## Error tracking example (Python)

https://sentry.io/

< URL >/ - Would respond with "Hello World" in html

< URL >/error - Would give ZeroDivisionError which would be tracked by Sentry URL provided in variable.