from flask import Flask, render_template

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from os import environ

app = Flask(__name__)

sentry_dsn=environ.get('SECRET_SENTRY_DSN')
#dsn='http://' + sentry_public_dsn + ':' + sentry_private_dsn + '@' + sentry_url + '/' + sentry_project_url

sentry_sdk.init(
    sentry_dsn,
    integrations=[FlaskIntegration()]
)

@app.route('/error')
def hello_error():

    1 / 0 #ZeroDivisionError to be sent to sentry
    return render_template("hello_world.html")

@app.route('/')
def hello():
    return render_template("hello_world.html")

if __name__ == '__main__':
    app.run(debug=True,host= '0.0.0.0',port=5000)